<style media="screen">
  body{
    background-image: url("assets/image/fondo.avif");
  }
  .circular--square {
    width: 100%;
    height: 200px; /* Ajusta la altura deseada */
    background-color: #f00; /* Cambia esto al color que desees */
    border-radius: 10px; /* Ajusta el radio de la esquina */
    display: flex;
    align-items: center;
    justify-content: center;
    color: white;
    overflow: hidden; /* Asegura que la imagen no se desborde */
  }
  .card {
    margin-top: 30px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    transition: transform 0.3s;
    border-radius: 10px; /* Ajusta el radio de la esquina */
  }
  .card:hover {
    transform: scale(1.05);
  }
  .card-title {
    font-size: 20px;
    color: #333;
    text-align: center; /* Centra el texto */
  }
  .card-text {
    font-size: 16px;
    color: #666;
    text-align: justify; /* Justifica el texto */
  }
</style>

<div class="row">
  <div class="col-md-2">
    <center>
    <img src="<?php echo base_url(); ?>/assets/image/logoicon.png" alt="" width="80px" height="auto">
  </center>
  </div>
</div>
<br>

<center>

<div class="row">
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria1.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria1.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"Explora nuevos horizontes y descubre oportunidades ilimitadas."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria2.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria2.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La inspiración está en todas partes, solo necesitas mirar con atención."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria3.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria3.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La innovación es la clave para destacar en un mundo en constante cambio."</p>
      </div>
    </center>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria4.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria4.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"Conecta con el mundo que te rodea y crea lazos duraderos."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria5.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria5.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La belleza está en los detalles que nos rodean cada día."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria6.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria6.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La creatividad es el motor que impulsa el progreso y la evolución."</p>
      </div>
    </center>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria7.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria7.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"Descubre la magia que se esconde en cada rincón del mundo."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria8.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria8.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La vitalidad y el entusiasmo son la fuerza que impulsa el éxito."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria9.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria9.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"Una nueva perspectiva puede abrir puertas que ni siquiera sabías que existían."</p>
      </div>
    </center>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria10.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria10.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"Encuentra la armonía entre tus sueños y la realidad que deseas crear."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria11.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria11.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"Energiza tu vida con pasión y determinación para alcanzar tus metas."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria12.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria12.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La pasión es el ingrediente secreto que da sabor a la vida."</p>
      </div>
    </center>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria13.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria13.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La esperanza es la luz que guía nuestros pasos en los momentos más oscuros."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria14.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria14.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La transformación comienza cuando te atreves a creer en ti mismo."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria15.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria15.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"Empodérate y haz que cada día cuente, porque tu potencial es infinito."</p>
      </div>
    </center>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria16.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria16.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"Asómbrate con las maravillas que el mundo tiene para ofrecer."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria17.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria17.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"Cada día es una aventura que espera ser explorada."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria18.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria18.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"Encuentra la libertad en la capacidad de ser tú mismo en todo momento."</p>
      </div>
    </center>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria19.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria19.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La gratitud es la llave que abre las puertas de la felicidad."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria20.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria20.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La sabiduría se encuentra en las lecciones que aprendemos en nuestro camino."</p>
      </div>
    </center>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <center>
      <a href="<?php echo base_url(); ?>/assets/image/galeria/galeria5.jpg"><img class="circular--square" src="<?php echo base_url(); ?>/assets/image/galeria/galeria5.jpg" class="card-img-top" alt="..."></a>
      <div class="card-body">
        <p class="card-text">"La renovación es el primer paso hacia un nuevo comienzo."</p>
      </div>
    </center>
    </div>
  </div>
</div>
</center>
