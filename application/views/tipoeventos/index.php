<style>
  .card {
    border: 1px solid white;
  }
</style>

<?php
$totalTipoeventos = 0;
$mostSeatsStadium = null;

if ($listadoTipoeventos) {
    $totalTipoeventos = sizeof($listadoTipoeventos);
    $maxSeats = PHP_INT_MIN;

    foreach ($listadoTipoeventos as $tipoeventoTemporal) {
    }
}
?>

<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE TIPO DE EVENTOS</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('tipoeventos/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i> Agregar evento</a>
</center>
<br>
</div>
<br>
<?php if ($listadoTipoeventos): ?>

  <div class="table-responsive" style="margin: 2 120px">
    <table class="table table-striped table-bordered table-hover" id="tbl_tipoeventos">
      <thead>
        <tr>
          <th>ID</th>
          <th>NOMBRE</th>
          <th>ESTADO</th>
          <th>CREACION</th>
          <th>ACTUALIZACION</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoTipoeventos as $tipoeventoTemporal): ?>
          <tr>
            <td><?php echo $tipoeventoTemporal->id_te ?></td>
            <td><?php echo $tipoeventoTemporal->nombre_te ?></td>
            <td><?php echo $tipoeventoTemporal->estado_te ?></td>
            <td><?php echo $tipoeventoTemporal->creacion_te ?></td>
            <td><?php echo $tipoeventoTemporal->actualizacion_te ?></td>

            <td class="text-center">
              <a href="<?php echo site_url(); ?>/tipoeventos/actualizar/<?php echo $tipoeventoTemporal->id_te; ?>" title="Editar evento">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Edit
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/tipoeventos/borrar/<?php echo $tipoeventoTemporal->id_te; ?>" title="Eliminar evento" onclick="return confirm('Are you sure to delete permanently?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Delete
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>There are no places</h1>
<?php endif; ?>

<br>
<div class="row" style="margin: 0 120px;">


  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          <img src="<?php echo base_url(); ?>/assets/image/kpi1.png" alt="" width="250" height="200">
          <?php echo $totalTipoeventos; ?>
        </h5>
        <p class="card-text">Registro de evento</p>
      </div>
    </div>
  </div>
</div>
<br>

<script type="text/javascript">
  $("#tbl_tipoeventos").DataTable();
</script>
