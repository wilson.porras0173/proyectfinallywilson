<h1 class="text-center"><i class=""></i> ACTUALIZAR DATOS</h1>
<form class=""
id="frm_actualizar_consumo"
action="<?php echo site_url('consumos/procesarActualizacion'); ?>"
method="post"
enctype="multipart/form-data">
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
          <div class="col-md-4">
            <input type="hidden" name="id_consumo" id="id_consumo" value="<?php echo $consumoEditar->id_consumo; ?>">
              <label for="">AÑO:
                <span class="obligatorio">(Required)</span>
              </label>
              <br>
              <input type="text"
              placeholder="Ingrese el año"
              class="form-control"
              required
              name="anio_consumo" value="<?php echo $consumoEditar->anio_consumo; ?>"
              id="anio_consumo">
              </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="mes_consumo">MES:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el mes" class="form-control" required name="mes_consumo"  value="<?php echo $consumoEditar->mes_consumo; ?>" id="mes_consumo">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="estado_consumo">ESTADO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el estado" class="form-control" required name="estado_consumo" value="<?php echo $consumoEditar->estado_consumo; ?>" id="estado_consumo">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha_creacion_consumo">FECHA CREACIÓN:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la fecha de creacion" class="form-control" required name="fecha_creacion_consumo" value="<?php echo $consumoEditar->fecha_creacion_consumo; ?>" id="fecha_creacion_consumo">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <label for="fecha_actualizacion_consumo">FECHA ACTUALIZACIÓN:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la fecha de actualizacion" class="form-control" required name="fecha_actualizacion_consumo" value="<?php echo $consumoEditar->fecha_actualizacion_consumo; ?>" id="fecha_actualizacion_consumo">
            </div>
        </div>
    </div>
</center>
    <br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="numero_mes_consumo">NUMERO MES:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese el numero del mes" class="form-control" required name="numero_mes_consumo" value="<?php echo $consumoEditar->numero_mes_consumo; ?>" id="numero_mes_consumo">
                </div>
            </div>
            <div class="col-md-1">
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="fecha_vencimiento_consumo">FECHA VENCIMIENTO:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese la fecha de vencimiento" class="form-control" required name="fecha_vencimiento_consumo" value="<?php echo $consumoEditar->fecha_vencimiento_consumo; ?>" id="fecha_vencimiento_consumo">
                </div>
            </div>
        </div>
    </center>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br><br>
            <button type="submit" name="button"
            class="btn btn-success">
            <i class="glyphicon glyphicon-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/consumos/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-ban-circle"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_actualizar_consumos").validate({
   rules:{
     anio_consumo:{
         required:true,
     },
     mes_consumo:{
       required:true,
     },
     estado_consumo:{
       required:true,
     },
     fecha_creacion_consumo:{
       required:true,
     },
     fecha_actualizacion_consumo:{
       required:true,
     },
     numero_mes_consumo:{
       required:true,
     },
     fecha_vencimiento_consumo:{
       required:true,
     }
   },
   messages:{
     anio_consumo:{
         required:"Porfavor, este campo solo admite numeros",
     },
     mes_consumo:{
         required:"Porfavor, este campo solo admite numeros",
     },
     estado_consumo:{
       required:"Porfavor, este campo solo admite letras",
     },
     fecha_creacion_consumo:{
       required:"Porfavor, este campo solo admite numeros",
     },
     fecha_actualizacion_consumo:{
         required:"Porfavor, este campo solo admite numeros",
     },
     numero_mes_consumo:{
         required:"Porfavor, este campo solo admite numeros",
     },
     fecha_vencimiento_consumo:{
       required:"Porfavor, este campo solo admite numeros",
     }
   }
 });

 </script>
