<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tu Página Mejorada</title>
    <style>
        /* Estilos para el encabezado */
        .header {
            background-color: #f0f0f0;
            padding: 20px 0;
            text-align: center;
            margin-bottom: 30px;
        }

        .header h4 {
            font-size: 24px;
            color: #333;
            margin-bottom: 10px;
        }

        .header p {
            font-size: 16px;
            color: #666;
        }

        /* Estilos para las tarjetas */
        .card {
            margin-top: 30px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            transition: transform 0.3s;
            height: 100%;
        }

        .card:hover {
            transform: scale(1.05);
        }

        .card-title {
            font-size: 20px;
            color: #333;
            text-align: center;
        }

        .card-text {
            font-size: 16px;
            color: #666;
            text-align: justify;
        }

        /* Estilos para las imágenes */
        .card-img-top {
            width: 100%;
            height: 400px; /* Altura fija para todas las imágenes */
            object-fit: cover; /* Ajustar la imagen para cubrir el tamaño especificado */
        }
    </style>
</head>
<body>
  <div class="row">
    <div class="col-md-2">
      <center>
      <img src="<?php echo base_url(); ?>/assets/image/logoicon.png" alt="" width="80px" height="auto">
    </center>
    </div>
  </div>
  <br>
<!-- Encabezado -->
<div class="header">
    <h4>"Emprendiendo Caminos de Éxito: Rutas de Esperanza"</h4>
    <p>Bienvenido a nuestro proyecto que se sumerge en el fascinante y diverso mundo de los negocios personales. Descubre historias inspiradoras, estrategias exitosas y la pasión que impulsa a las personas a alcanzar sus metas.</p>
</div>

<!-- Contenido de las tarjetas -->
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <img src="<?php echo base_url(); ?>/assets/image/mision.png" class="card-img-top" alt="mision">
                <div class="card-body">
                    <h5 class="card-title">MISIÓN</h5>
                    <p class="card-text">Nuestra misión es proporcionar a los principales entes gubernamentales una plataforma confiable y fácil de usar que les permita encontrar y ubicar fácilmente negocios locales a través de la georreferenciación, promoviendo así el crecimiento económico y facilitando la conexión entre consumidores y empresa.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img src="<?php echo base_url(); ?>/assets/image/vision.jpg" class="card-img-top" alt="vision">
                <div class="card-body">
                    <h5 class="card-title">VISIÓN</h5>
                    <p class="card-text">Nuestra visión es convertirnos en la principal herramienta de georreferenciación de negocios a nivel local, brindando a las comunidades la capacidad de descubrir, apoyar y conectar de manera eficiente con las empresas locales, promoviendo así la prosperidad económica y el desarrollo sostenible.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img src="<?php echo base_url(); ?>/assets/image/objetivo.jpg" class="card-img-top" alt="objetivo">
                <div class="card-body">
                    <h5 class="card-title">OBJETIVO</h5>
                    <p class="card-text">Contribuir activamente a la formulación e implementación de políticas económicas que promuevan un crecimiento sostenible e inclusivo es fundamental para nuestro proyecto. Nos comprometemos a trabajar en colaboración con las autoridades pertinentes y las partes interesadas para identificar y abordar las necesidades de la comunidad empresarial local. </p>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
