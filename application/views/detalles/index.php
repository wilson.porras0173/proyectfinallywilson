<style>
  .card {
    border: 1px solid white;
  }
</style>

<?php
$totalDetalles = 0;
$mostSeatsStadium = null;

if ($listadoDetalles) {
    $totalDetalles = sizeof($listadoDetalles);
    $maxSeats = PHP_INT_MIN;

    foreach ($listadoDetalles as $detalleTemporal) {
    }
}
?>

<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE DETALLES</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('detalles/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i> Agregar Detalle</a>
</center>
<br>
</div>
<br>
<?php if ($listadoDetalles): ?>

  <div class="table-responsive" style="margin: 2 120px">
    <table class="table table-striped table-bordered table-hover" id="tbl_detalles">
      <thead>
        <tr>
          <th>ID</th>
          <th>FK_ID_LEC</th>
          <th>FK_ID_REC</th>
          <th>CANTIDAD</th>
          <th>DETALLE</th>
          <th>VALOR UNITARIO</th>
          <th>SUBTOTAL</th>
          <th>IVA</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoDetalles as $detalleTemporal): ?>
          <tr>
            <td><?php echo $detalleTemporal->id_det ?></td>
            <td><?php echo $detalleTemporal->fk_id_lec ?></td>
            <td><?php echo $detalleTemporal->fk_id_rec ?></td>
            <td><?php echo $detalleTemporal->cantidad_det ?></td>
            <td><?php echo $detalleTemporal->detalle_det ?></td>
            <td><?php echo $detalleTemporal->valor_unitario_det ?></td>
            <td><?php echo $detalleTemporal->subtotal_det ?></td>
            <td><?php echo $detalleTemporal->iva_det ?></td>

            <td class="text-center">
              <a href="<?php echo site_url(); ?>/detalles/actualizar/<?php echo $detalleTemporal->id_det; ?>" title="Editar Detalle">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Edit
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/detalles/borrar/<?php echo $detalleTemporal->id_det; ?>" title="Eliminar detalle" onclick="return confirm('Are you sure to delete permanently?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Delete
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>There are no places</h1>
<?php endif; ?>

<br>
<div class="row" style="margin: 0 120px;">


  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          <img src="<?php echo base_url(); ?>/assets/image/kpi1.png" alt="" width="250" height="200">
          <?php echo $totalDetalles; ?>
        </h5>
        <p class="card-text">Registro de Detalles</p>
      </div>
    </div>
  </div>
</div>
<br>

<script type="text/javascript">
  $("#tbl_detalles").DataTable();
</script>
