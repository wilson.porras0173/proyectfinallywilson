<style>
  .card {
    border: 1px solid white;
  }
</style>

<?php
$totalEventos = 0;
$mostSeatsStadium = null;

if ($listadoEventos) {
    $totalEventos = sizeof($listadoEventos);
    $maxSeats = PHP_INT_MIN;

    foreach ($listadoEventos as $eventoTemporal) {
    }
}
?>

<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE EVENTOS</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('eventos/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i> Agregar Evento</a>
</center>
<br>
</div>
<br>
<?php if ($listadoEventos): ?>

  <div class="table-responsive" style="margin: 2 120px">
    <table class="table table-striped table-bordered table-hover" id="tbl_eventos">
      <thead>
        <tr>
          <th>ID</th>
          <th>DESCRIPCION</th>
          <th>FECHA_HORA</th>
          <th>LUGAR</th>
          <th>FK_ID_TE</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoEventos as $eventoTemporal): ?>
          <tr>
            <td><?php echo $eventoTemporal->id_eve ?></td>
            <td><?php echo $eventoTemporal->descripcion_eve ?></td>
            <td><?php echo $eventoTemporal->fecha_hora_eve ?></td>
            <td><?php echo $eventoTemporal->lugar_eve ?></td>
            <td><?php echo $eventoTemporal->fk_id_te ?></td>

            <td class="text-center">
              <a href="<?php echo site_url(); ?>/eventos/actualizar/<?php echo $eventoTemporal->id_eve; ?>" title="Editar Evento">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Edit
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/eventos/borrar/<?php echo $eventoTemporal->id_eve; ?>" title="Eliminar perfil" onclick="return confirm('Are you sure to delete permanently?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Delete
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>There are no places</h1>
<?php endif; ?>

<br>
<div class="row" style="margin: 0 120px;">


  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          <img src="<?php echo base_url(); ?>/assets/image/kpi1.png" alt="" width="250" height="200">
          <?php echo $totalEventos; ?>
        </h5>
        <p class="card-text">Registro de Eventos</p>
      </div>
    </div>
  </div>
</div>
<br>

<script type="text/javascript">
  $("#tbl_eventos").DataTable();
</script>
