<style>
  .card {
    border: 1px solid white;
  }
</style>

<?php
$totalSocios = 0;
$mostSeatsStadium = null;

if ($listadoSocios) {
    $totalSocios = sizeof($listadoSocios);
    $maxSeats = PHP_INT_MIN;

    foreach ($listadoSocios as $socioTemporal) {
    }
}
?>

<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE SOCIOS</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('socios/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i> Agregar Socio</a>
</center>
<br>
</div>
<br>
<?php if ($listadoSocios): ?>

  <div class="table-responsive" style="margin: 2 120px">
    <table class="table table-striped table-bordered table-hover" id="tbl_socios">
      <thead>
        <tr>
          <th>ID</th>
          <th>TIPO</th>
          <th>IDENTIFICACION</th>
          <th>PRIMER APELLIDO</th>
          <th>SEGUNDO APELLIDO</th>
          <th>NOMBRES</th>
          <th>EMAIL</th>
          <th>FOTO</th>
          <th>TELEFONO</th>
          <th>DIRECCION</th>
          <th>FECHA NACIMIENTO</th>
          <th>DISCAPACIDAD</th>
          <th>FK_ID_USU</th>
          <th>ESTADO</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoSocios as $socioTemporal): ?>
          <tr>
            <td><?php echo $socioTemporal->id_soc ?></td>
            <td><?php echo $socioTemporal->tipo_soc ?></td>
            <td><?php echo $socioTemporal->identificacion_soc ?></td>
            <td><?php echo $socioTemporal->primer_apellido_soc ?></td>
            <td><?php echo $socioTemporal->segundo_apellido_soc ?></td>
            <td><?php echo $socioTemporal->nombres_soc ?></td>
            <td><?php echo $socioTemporal->email_soc ?></td>
            <td><?php echo $socioTemporal->foto_soc ?></td>
            <td><?php echo $socioTemporal->telefono_soc ?></td>
            <td><?php echo $socioTemporal->direccion_soc ?></td>
            <td><?php echo $socioTemporal->fecha_nacimiento_soc ?></td>
            <td><?php echo $socioTemporal->discapacidad_soc ?></td>
            <td><?php echo $socioTemporal->fk_id_usu ?></td>
            <td><?php echo $socioTemporal->estado_soc ?></td>

            <td class="text-center">
              <a href="<?php echo site_url(); ?>/socios/actualizar/<?php echo $socioTemporal->id_soc; ?>" title="Editar Socio">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Edit
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/socios/borrar/<?php echo $socioTemporal->id_soc; ?>" title="Eliminar socio" onclick="return confirm('Are you sure to delete permanently?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Delete
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>There are no places</h1>
<?php endif; ?>

<br>
<div class="row" style="margin: 0 120px;">


  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          <img src="<?php echo base_url(); ?>/assets/image/kpi1.png" alt="" width="250" height="200">
          <?php echo $totalSocios; ?>
        </h5>
        <p class="card-text">Registro de Socios</p>
      </div>
    </div>
  </div>
</div>
<br>

<script type="text/javascript">
  $("#tbl_socios").DataTable();
</script>
