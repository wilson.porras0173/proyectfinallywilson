<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVO SOCIO</h1>
<form class="" id="frm_nuevo_socio" action="<?php echo site_url('socios/guardarSocio'); ?>" method="post" enctype="multipart/form-data">

<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="tipo_soc">TIPO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el tipo" class="form-control" required name="tipo_soc" id="tipo_soc">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="identificacion_soc">IDENTIFICACION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese una identificacion" class="form-control" required name="identificacion_soc" id="identificacion_soc">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="primer_apellido_soc">PRIMER APELLIDO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el primer apellido" class="form-control" required name="primer_apellido_soc" id="primer_apellido_soc">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="segundo_apellido_soc">SEGUNDO APELLIDO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el segundo apellido" class="form-control" required name="segundo_apellido_soc" id="segundo_apellido_soc">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="nombres_soc">NOMBRES:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el nombre" class="form-control" required name="nombres_soc" id="nombres_soc">
            </div>
        </div>
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="email_soc">EMAIL:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el email" class="form-control" required name="email_soc" id="email_soc">
            </div>
        </div>
    </div>
</center>
    <br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="foto_soc">FOTO:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese una foto" class="form-control" required name="foto_soc" id="foto_soc">
                </div>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="telefono_soc">TELEFONO:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese el # telefonico" class="form-control" required name="telefono_soc" id="telefono_soc">
                </div>
            </div>
        </div>
    </center>
        <br>
        <center>
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="direccion_soc">DIRECCION:
                            <span class="obligatorio">(Required)</span>
                        </label>
                        <input type="text" placeholder="Ingrese la direccion" class="form-control" required name="direccion_soc" id="direccion_soc">
                    </div>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="fecha_nacimiento_soc">FECHA NACIMIENTO:
                            <span class="obligatorio">(Required)</span>
                        </label>
                        <input type="text" placeholder="Ingrese la fecha de nacimiento" class="form-control" required name="fecha_nacimiento_soc" id="fecha_nacimiento_soc">
                    </div>
                </div>
            </div>
        </center>
            <br>
            <center>
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="discapacidad_soc">DISCAPACIDAD:
                                <span class="obligatorio">(Required)</span>
                            </label>
                            <input type="text" placeholder="Ingrese la discapacidad" class="form-control" required name="discapacidad_soc" id="discapacidad_soc">
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="fk_id_usu">FK_ID_USU:
                                <span class="obligatorio">(Required)</span>
                            </label>
                            <input type="text" placeholder="Ingrese el id de usuario" class="form-control" required name="fk_id_usu" id="fk_id_usu">
                        </div>
                    </div>
                </div>
            </center>
                <br>
                <center>
                    <div class="row">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="estado_soc">ESTADO:
                                    <span class="obligatorio">(Required)</span>
                                </label>
                                <input type="text" placeholder="Ingrese el estado" class="form-control" required name="estado_soc" id="estado_soc">
                            </div>
                        </div>
                        <div class="col-md-1">
                        </div>

                    </div>
                </center>
                    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-success">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/registros/index" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban-circle"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_socio").validate({
    rules:{
        tipo_soc:{
            required:true,
        },
        identificacion_soc:{
            required:true,
        },
        primer_apellido_soc:{
          required:true,
        },
        segundo_apellido_soc:{
          required:true,
        },
        nombres_soc:{
          required:true,
        },
        email_soc:{
          required:true,
        },
        foto_soc:{
          required:true,
        },
        telefono_soc:{
          required:true,
        },
        direccion_soc:{
          required:true,
        },
        fecha_nacimiento_soc:{
          required:true,
        },
        discapacidad_soc:{
          required:true,
        },
        fk_id_usu:{
          required:true,
        },
        estado_soc:{
          required:true,
        }
    },
    messages:{
        tipo_soc:{
            required:"Porfavor, este campo solo admite letras",
        },
        identificacion_soc:{
            required:"Porfavor, este campo solo admite numeros",
        },
        primer_apellido_soc:{
          required:"Porfavor, este campo solo admite letras",
        },
        segundo_apellido_soc:{
          required:"Porfavor, este campo solo admite letra",
        },
        nombres_soc:{
          required:"Porfavor, este campo solo admite letras",
        },
        email_soc:{
          required:"Porfavor, este campo solo admite letras, numeros y caracteres especiales",
        },
        foto_soc:{
          required:"Porfavor, este campo solo admite letras",
        },
        telefono_soc:{
          required:"Porfavor, este campo solo admite numeros",
        },
        direccion_soc:{
          required:"Porfavor, este campo solo admite letras",
        },
        fecha_nacimiento_soc:{
          required:"Porfavor, este campo solo admite numeros",
        },
        discapacidad_soc:{
          required:"Porfavor, este campo solo admite letras",
        },
        fk_id_usu:{
          required:"Porfavor, este campo solo admite numeros",
        },
        estado_soc:{
          required:"Porfavor, este campo solo admite letras",
        }
      }
});
</script>
