<style>
  .card {
    border: 1px solid white;
  }
</style>

<?php
$totalLecturas = 0;
$mostSeatsStadium = null;

if ($listadoLecturas) {
    $totalLecturas = sizeof($listadoLecturas);
    $maxSeats = PHP_INT_MIN;

    foreach ($listadoLecturas as $lecturaTemporal) {
    }
}
?>

<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE LECTURAS</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('lecturas/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i> Agregar Lectura</a>
</center>
<br>
</div>
<br>
<?php if ($listadoLecturas): ?>

  <div class="table-responsive" style="margin: 2 120px">
    <table class="table table-striped table-bordered table-hover" id="tbl_lecturas">
      <thead>
        <tr>
          <th>ID</th>
          <th>AÑO</th>
          <th>MES</th>
          <th>ESTADO</th>
          <th>LECTURA ANTERIOR</th>
          <th>LECTURA ACTUAL</th>
          <th>FECHA CREACION</th>
          <th>FECHA ACTUALIZACION</th>
          <th>FK_ID_HIS</th>
          <th>FK_ID_CONSUMO</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoLecturas as $lecturaTemporal): ?>
          <tr>
            <td><?php echo $lecturaTemporal->id_lec ?></td>
            <td><?php echo $lecturaTemporal->anio_lec ?></td>
            <td><?php echo $lecturaTemporal->mes_lec ?></td>
            <td><?php echo $lecturaTemporal->estado_lec ?></td>
            <td><?php echo $lecturaTemporal->lectura_anterior_lec ?></td>
            <td><?php echo $lecturaTemporal->lectura_actual_lec ?></td>
            <td><?php echo $lecturaTemporal->fecha_creacion_lec ?></td>
            <td><?php echo $lecturaTemporal->fecha_actualizacion_lec ?></td>
            <td><?php echo $lecturaTemporal->fk_id_his ?></td>
            <td><?php echo $lecturaTemporal->fk_id_consumo ?></td>

            <td class="text-center">
              <a href="<?php echo site_url(); ?>/lecturas/actualizar/<?php echo $lecturaTemporal->id_lec; ?>" title="Editar Lectura">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Edit
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/lecturas/borrar/<?php echo $lecturaTemporal->id_lec; ?>" title="Eliminar lectura" onclick="return confirm('Are you sure to delete permanently?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Delete
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>There are no places</h1>
<?php endif; ?>

<br>
<div class="row" style="margin: 0 120px;">


  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          <img src="<?php echo base_url(); ?>/assets/image/kpi1.png" alt="" width="250" height="200">
          <?php echo $totalLecturas; ?>
        </h5>
        <p class="card-text">Registro de Lectura</p>
      </div>
    </div>
  </div>
</div>
<br>

<script type="text/javascript">
  $("#tbl_lecturas").DataTable();
</script>
