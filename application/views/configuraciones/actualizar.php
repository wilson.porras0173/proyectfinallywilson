<h1 class="text-center"><i class=""></i> ACTUALIZAR DATOS</h1>
<form class=""
id="frm_actualizar_configuracion"
action="<?php echo site_url('configuraciones/procesarActualizacion'); ?>"
method="post"
enctype="multipart/form-data">
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
          <div class="col-md-4">
            <input type="hidden" name="id_con" id="id_con" value="<?php echo $configuracionEditar->id_con; ?>">
              <label for="">NOMBRE:
                <span class="obligatorio">(Required)</span>
              </label>
              <br>
              <input type="text"
              placeholder="Ingrese el nombre"
              class="form-control"
              required
              name="nombre_con" value="<?php echo $configuracionEditar->nombre_con; ?>"
              id="nombre_con">
              </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="ruc_con">RUC:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el ruc" class="form-control" required name="ruc_con"  value="<?php echo $configuracionEditar->ruc_con; ?>" id="ruc_con">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="logo_con">LOGO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el logo" class="form-control" required name="logo_con" value="<?php echo $configuracionEditar->logo_con; ?>" id="logo_con">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="telefono_con">TELEFONO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese un numero telefonico" class="form-control" required name="telefono_con" value="<?php echo $configuracionEditar->telefono_con; ?>" id="telefono_con">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <label for="direccion_con">DIRECCION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la direccion" class="form-control" required name="direccion_con" value="<?php echo $configuracionEditar->direccion_con; ?>" id="direccion_con">
            </div>
        </div>
    </div>
</center>
    <br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email_con">EMAIL:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese el email" class="form-control" required name="email_con" value="<?php echo $configuracionEditar->email_con; ?>" id="email_con">
                </div>
            </div>
            <div class="col-md-1">
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="servidor_con">SERVIDOR:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese un servidor" class="form-control" required name="servidor_con" value="<?php echo $configuracionEditar->servidor_con; ?>" id="servidor_con">
                </div>
            </div>
        </div>
    </center>
    <br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="puerto_con">PUERTO:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese el puerto" class="form-control" required name="puerto_con" value="<?php echo $configuracionEditar->puerto_con; ?>" id="puerto_con">
                </div>
            </div>
            <div class="col-md-1">
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="password_con">PASSWORD:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese una contraseña" class="form-control" required name="password_con" value="<?php echo $configuracionEditar->password_con; ?>" id="password_con">
                </div>
            </div>
        </div>
    </center>
    <br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="creacion_con">CREACION:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese la creacion de configuracion" class="form-control" required name="creacion_con" value="<?php echo $configuracionEditar->creacion_con; ?>" id="creacion_con">
                </div>
            </div>
            <div class="col-md-1">
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="actualizacion_con">ACTUALIZACION:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese la actualizacion de configuracion" class="form-control" required name="actualizacion_con" value="<?php echo $configuracionEditar->actualizacion_con; ?>" id="actualizacion_con">
                </div>
            </div>
        </div>
    </center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="anio_inicial_con">AÑO INICIAL:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el año inicial" class="form-control" required name="anio_inicial_con" value="<?php echo $configuracionEditar->anio_inicial_con; ?>" id="anio_inicial_con">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="mes_inicial_con">MES INICIAL:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el mes inicial" class="form-control" required name="mes_inicial_con" value="<?php echo $configuracionEditar->mes_inicial_con; ?>" id="mes_inicial_con">
            </div>
        </div>
    </div>
</center>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br><br>
            <button type="submit" name="button"
            class="btn btn-success">
            <i class="glyphicon glyphicon-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/configuraciones/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-ban-circle"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_actualizar_configuraciones").validate({
   rules:{
     nombre_con:{
       required:true
     },
     ruc_con:{
       required:true
     },
     logo_con:{
       required:true
     },
     telefono_con:{
       required:true
     },
     direccion_con:{
       required:true
     },
     email_con:{
       required:true
     },
     servidor_con:{
       required:true
     },
     puerto_con:{
       required:true
     },
     password_con:{
       required:true
     },
     creacion_con:{
       required:true
     },
     actualizacion_con:{
       required:true
     },
     anio_inicial_con:{
       required:true
     },
     mes_inicial_con:{
       required:true
     }
   },
   messages:{
     nombre_con:{
         required:"Porfavor, este campo solo admite letras",
     },
     ruc_con:{
         required:"Porfavor, este campo solo admite numeros",
     },
     logo_con:{
       required:"Porfavor, este campo solo admite letras",
     },
     telefono_con:{
       required:"Porfavor, este campo solo admite numeros",
     },
     direccion_con:{
       required:"Porfavor, este campo admite letras, numeros y caracteres especiales",
     },
     email_con:{
       required:"Porfavor, este campo admite letras, numeros y caracteres especiales",
     },
     servidor_con:{
       required:"Porfavor, este campo solo admite numeros",
     },
     puerto_con:{
       required:"Porfavor, este campo solo admite numeros",
     },
     password_con:{
       required:"Porfavor, este campo admite letras, numeros y caracteres especiales",
     },
     creacion_con:{
       required:"Porfavor, este campo solo admite numeros",
     },
     actualizacion_con:{
       required:"Porfavor, este campo solo admite numeros",
     },
     anio_inicial_con:{
       required:"Porfavor, este campo solo admite numeros",
     },
     mes_inicial_con:{
       required:"Porfavor, este campo solo admite letras",
     }
   }
 });

 </script>
