
    <style>
        /* Estilos para el encabezado */
        .header {
            background-color: #f0f0f0;
            padding: 20px 0;
            text-align: center;
        }

        .header h4 {
            font-size: 24px;
            color: #333;
            margin-bottom: 10px;
        }

        .header p {
            font-size: 16px;
            color: #666;
        }

        /* Estilos para las tarjetas */
        .card {
            margin-top: 30px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            transition: transform 0.3s;
        }

        .card:hover {
            transform: scale(1.05);
        }

        .card-title {
            font-size: 20px;
            color: #333;
        }

        .card-text {
            font-size: 16px;
            color: #666;
        }
    </style>


<!-- Encabezado -->
<div class="header">
    <h4>"Emprendiendo Caminos de Éxito: Rutas de Esperanzas"</h4>
    <p>Bienvenido a nuestro proyecto que se sumerge en el fascinante y diverso mundo de los negocios personales. Descubre historias inspiradoras, estrategias exitosas y la pasión que impulsa a las personas a alcanzar sus metas.</p>
</div>

<!-- Contenido de las tarjetas -->
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <img src="<?php echo base_url(); ?>/assets/image/mision.png" class="card-img-top" alt="mision">
                <div class="card-body">
                    <h5 class="card-title">MISIÓN</h5>
                    <p class="card-text">Nuestra misión es proporcionar a los principales entes gubernamentales una plataforma confiable y fácil de usar que les permita encontrar y ubicar fácilmente negocios locales a través de la georreferenciación, promoviendo así el crecimiento económico y facilitando la conexión entre consumidores y empresa.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img src="<?php echo base_url(); ?>/assets/image/vision.jpg" class="card-img-top" alt="vision">
                <div class="card-body">
                    <h5 class="card-title">VISIÓN</h5>
                    <p class="card-text">Nuestra visión es convertirnos en la principal herramienta de georreferenciación de negocios a nivel local, brindando a las comunidades la capacidad de descubrir, apoyar y conectar de manera eficiente con las empresas locales, promoviendo así la prosperidad económica y el desarrollo sostenible.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img src="<?php echo base_url(); ?>/assets/image/objetivo.jpg" class="card-img-top" alt="objetivo">
                <div class="card-body">
                    <h5 class="card-title">OBJETIVO</h5>
                    <p class="card-text">Contribuir activamente a la formulación e implementación de políticas económicas que promuevan un crecimiento sostenible e inclusivo.</p>
                </div>
            </div>
        </div>
    </div>
</div>
