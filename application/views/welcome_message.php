<style media="screen">

.colorful-background {
    background: linear-gradient(135deg, #ff0000, #0000ff); /* Gradiente de colores rojo y azul */
    padding: 50px 0; /* Ajusta el relleno según sea necesario */
    color: white; /* Color de texto blanco para que se contraste con los colores de fondo */
}

</style>

<br>
<!-- Carousel Start -->
<div class="container-fluid p-0 mb-5 pb-5">
    <div id="header-carousel" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#header-carousel" data-slide-to="1"></li>
            <li data-target="#header-carousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item position-relative active" style="min-height: 100vh;">
                <img class="position-absolute w-100 h-100" src="<?php echo base_url();?>/assets/image/carousel1.0.jpg" style="object-fit: cover;">
                <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                    <div class="p-3" style="max-width: 900px;">
                        <h6 class="text-white text-uppercase mb-3 animate__animated animate__fadeInDown" style="letter-spacing: 3px;">"Forjando el futuro digital: Sistemas de Información, el corazón de la innovación."</h6>
                        <h5 class="display-6 text-capitalize text-white mb-3">"En la carrera de Sistemas de Información, convertimos datos en oportunidades, códigos en soluciones y sueños en realidad".</h5>
                    </div>
                </div>
            </div>
            <div class="carousel-item position-relative" style="min-height: 100vh;">
                <img class="position-absolute w-100 h-100" src="<?php echo base_url();?>/assets/image/carousel1.0.1.jpg" style="object-fit: cover;">
                <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                    <div class="p-3" style="max-width: 900px;">
                        <h6 class="text-white text-uppercase mb-3 animate__animated animate__fadeInDown" style="letter-spacing: 3px;">"Descifrando el futuro: Sistemas de Información, el arte de transformar datos en posibilidades."</h6>
                        <h3 class="display-6 text-capitalize text-white mb-3">"En la carrera de Sistemas de Información, cada algoritmo es una nota en la sinfonía del progreso."</h3>
                        <p class="mx-md-5 px-5"></p>

                    </div>
                </div>
            </div>
            <div class="carousel-item position-relative" style="min-height: 100vh;">
                <img class="position-absolute w-100 h-100" src="<?php echo base_url();?>/assets/image/carousel1.0.2.jpg" style="object-fit: cover;">
                <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                    <div class="p-3" style="max-width: 900px;">
                        <h6 class="text-white text-uppercase mb-3 animate__animated animate__fadeInDown" style="letter-spacing: 3px;">"Construyendo puentes digitales: Sistemas de Información, el camino hacia la conectividad global"</h6>
                        <h3 class="display-6 text-capitalize text-white mb-3">"En la carrera de Sistemas de Información, cada línea de código es un ladrillo en el puente hacia un mundo más conectado."</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Carousel End -->
<!-- Carousel End -->
<center>
  <style>

      #emprende-con-proposito {
          animation: moveLeftRight 1s ease-in-out infinite alternate;
      }

      /* Definición de la animación de movimiento lateral */
      @keyframes moveLeftRight {
          0% {
              transform: translateX(-20px);
          }
          100% {
              transform: translateX(20px);
          }
      }
  </style>

  <!-- Título con movimiento -->
  <h1 id="emprende-con-proposito">"SISTEMAS DE INFORMACION PARA LA INTELIGENCIA DE NEGOCIOS"</h1>


<br>

<h1></h1>
</center>
<br>
<div class="row">
    <style>
        .card:hover {
            transform: scale(1.00);
        }

        .card img {
            border-radius: 10px 10px 0 0;
        }

        .card-body {
            text-align: center;
        }

        h1 {
            font-size: 1.5rem;
            margin-bottom: 10px;
        }

        p {
            font-size: 0.9rem;
            color: black;
            text-align: justify;
        }
        /* Estilo para el fondo ondulado de las tarjetas */
        .rainbow-background {
            background: radial-gradient(circle at 30% 60%,   white 80%, red 8%,  red 1%);
            padding: 20px; /* Ajusta el relleno según sea necesario */
            border-radius: 30px; /* Añade bordes redondeados */
            margin-bottom: 20px; /* Espaciado inferior entre tarjetas */
        }

    </style>

    <div class="card col-md-4 rainbow-background">
        <center>
            <img src="<?php echo base_url();?>/assets/image/contenido.webp" alt="..." style="width: 80%;">
        </center>
        <div class="card-body">
            <h1>"¿QUÉ?"</h1>
            <p class="card-text">""En la carrera de Sistemas de Información, te adentras en un emocionante mundo donde la innovación y la creatividad se fusionan para dar forma al futuro. Es un camino que te desafía constantemente a resolver problemas complejos y a encontrar soluciones ingeniosas que impacten positivamente en la sociedad."</p>
        </div>
    </div>

    <div class="card col-md-4 rainbow-background">
        <center>
            <img src="<?php echo base_url();?>/assets/image/contenido1.png"   alt="..." style="width: 95%;">
        </center>
        <div class="card-body">
            <h1>"¿CÓMO?"</h1>
            <p class="card-text">"La carrera de Sistemas de Información es un viaje fascinante hacia el corazón de la era digital, donde cada línea de código que escribes es un paso hacia la construcción de un mundo más eficiente y conectado. En este campo, tu habilidad para transformar datos en conocimiento y para crear soluciones innovadoras es la clave para resolver los desafíos más complejos de nuestra sociedad moderna."</p>
        </div>
    </div>

    <div class="card col-md-4 rainbow-background">
        <center>
            <img src="<?php echo base_url();?>/assets/image/contenido2.jpg"   alt="..." style="width: 95%;">
        </center>
        <div class="card-body">
            <h1>"¿Para Quién?"</h1>
            <p class="card-text">"La carrera de Sistemas de Información te invita a explorar un universo de posibilidades donde la imaginación y la tecnología convergen para dar forma a un mañana lleno de innovación y progreso. En este camino, cada línea de código es una oportunidad para crear, cada algoritmo es una puerta hacia nuevas soluciones y cada sistema que diseñas es un paso hacia un mundo más eficiente y conectado."</p>
        </div>
    </div>
</div>


<br>
<br>
<center>
  <style>

      #emprende-con-proposito {
          animation: moveLeftRight 1s ease-in-out infinite alternate;
      }

      /* Definición de la animación de movimiento lateral */
      @keyframes moveLeftRight {
          0% {
              transform: translateX(-20px);
          }
          100% {
              transform: translateX(20px);
          }
      }
  </style>

  <!-- Título con movimiento -->
  <h1 id="emprende-con-proposito">"SISTEMAS DE INFORMACION PARA LA INTELIGENCIA DE NEGOCIOS"</h1>

<br>
</center>
