<style>
  .card {
    border: 1px solid white;
  }
</style>

<?php
$totalRecaudaciones = 0;
$mostSeatsStadium = null;

if ($listadoRecaudaciones) {
    $totalRecaudaciones = sizeof($listadoRecaudaciones);
    $maxSeats = PHP_INT_MIN;

    foreach ($listadoRecaudaciones as $recaudacionTemporal) {
    }
}
?>

<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE RECAUDACIONES</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('recaudaciones/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i> Agregar Recaudacion</a>
</center>
<br>
</div>
<br>
<?php if ($listadoRecaudaciones): ?>

  <div class="table-responsive" style="margin: 2 120px">
    <table class="table table-striped table-bordered table-hover" id="tbl_recaudaciones">
      <thead>
        <tr>
          <th>ID</th>
          <th>NUMERO FACTURA</th>
          <th>NUMERO AUTORIZACION</th>
          <th>FECHA HORA AUTORIZACION</th>
          <th>AMBIENTE</th>
          <th>EMISION</th>
          <th>CLAVE ACCESO</th>
          <th>EMAIL</th>
          <th>OBSERVACION</th>
          <th>FK_ID_SOC</th>
          <th>NOMBRE</th>
          <th>IDENTIFICACION</th>
          <th>DIRECCION</th>
          <th>ESTADO</th>
          <th>FECHA EMISION</th>
          <th>FECHA CREACION</th>
          <th>FECHA ACTUALIZACION</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoRecaudaciones as $recaudacionTemporal): ?>
          <tr>
            <td><?php echo $recaudacionTemporal->id_rec ?></td>
            <td><?php echo $recaudacionTemporal->numero_factura_rec ?></td>
            <td><?php echo $recaudacionTemporal->numero_autorizacion_rec ?></td>
            <td><?php echo $recaudacionTemporal->fecha_hora_autorizacion_rec ?></td>
            <td><?php echo $recaudacionTemporal->ambiente_rec ?></td>
            <td><?php echo $recaudacionTemporal->emision_rev ?></td>
            <td><?php echo $recaudacionTemporal->clave_acceso_rec ?></td>
            <td><?php echo $recaudacionTemporal->email_rec ?></td>
            <td><?php echo $recaudacionTemporal->observacion_rec ?></td>
            <td><?php echo $recaudacionTemporal->fk_id_soc ?></td>
            <td><?php echo $recaudacionTemporal->nombre_rec ?></td>
            <td><?php echo $recaudacionTemporal->identificacion_rec ?></td>
            <td><?php echo $recaudacionTemporal->direccion_rec ?></td>
            <td><?php echo $recaudacionTemporal->estado_rec ?></td>
            <td><?php echo $recaudacionTemporal->fecha_emision_rec ?></td>
            <td><?php echo $recaudacionTemporal->fecha_creacion_rec ?></td>
            <td><?php echo $recaudacionTemporal->fecha_actualizacion_rec ?></td>
            <td class="text-center">
              <a href="<?php echo site_url(); ?>/recaudaciones/actualizar/<?php echo $recaudacionTemporal->id_rec; ?>" title="Editar Recaudacion">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Edit
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/recaudaciones/borrar/<?php echo $recaudacionTemporal->id_rec; ?>" title="Eliminar Recaudacion" onclick="return confirm('Are you sure to delete permanently?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Delete
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>There are no places</h1>
<?php endif; ?>

<br>
<div class="row" style="margin: 0 120px;">


  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          <img src="<?php echo base_url(); ?>/assets/image/kpi1.png" alt="" width="250" height="200">
          <?php echo $totalRecaudaciones; ?>
        </h5>
        <p class="card-text">Registro de Recaudacion</p>
      </div>
    </div>
  </div>
</div>
<br>

<script type="text/javascript">
  $("#tbl_recaudaciones").DataTable();
</script>
