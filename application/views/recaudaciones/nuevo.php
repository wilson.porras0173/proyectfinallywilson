<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVA RECAUDACION</h1>
<form class="" id="frm_nuevo_recaudacion" action="<?php echo site_url('recaudaciones/guardarRecaudacion'); ?>" method="post" enctype="multipart/form-data">

<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="numero_factura_rec">NUMERO FACTURA:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el numero de factura" class="form-control" required name="numero_factura_rec" id="numero_factura_rec">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="numero_autorizacion_rec">NUMERO AUTORIZACION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el numero de autorizacion" class="form-control" required name="numero_autorizacion_rec" id="numero_autorizacion_rec">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha_hora_autorizacion_rec">FECHA HORA AUTORIZACION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la fecha de autorizacion" class="form-control" required name="fecha_hora_autorizacion_rec" id="fecha_hora_autorizacion_rec">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="ambiente_rec">AMBIENTE:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el ambiente" class="form-control" required name="ambiente_rec" id="ambiente_rec">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="emision_rev">EMISION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la emision" class="form-control" required name="emision_rev" id="emision_rev">
            </div>
        </div>
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="clave_acceso_rec">CLAVE ACCESO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la clave de acceso" class="form-control" required name="clave_acceso_rec" id="clave_acceso_rec">
            </div>
        </div>
    </div>
</center>
    <br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email_rec">EMAIL:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese el email" class="form-control" required name="email_rec" id="email_rec">
                </div>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="observacion_rec">OBSERVACION:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese la observacion" class="form-control" required name="observacion_rec" id="observacion_rec">
                </div>
            </div>
        </div>
    </center>
        <br>
        <center>
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="fk_id_soc">FK_ID_SOC:
                            <span class="obligatorio">(Required)</span>
                        </label>
                        <input type="text" placeholder="Ingrese el id de socio" class="form-control" required name="fk_id_soc" id="fk_id_soc">
                    </div>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nombre_rec">NOMBRE:
                            <span class="obligatorio">(Required)</span>
                        </label>
                        <input type="text" placeholder="Ingrese el nombre " class="form-control" required name="nombre_rec" id="nombre_rec">
                    </div>
                </div>
            </div>
        </center>
            <br>
            <center>
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="identificacion_rec">IDENTIFICACION:
                                <span class="obligatorio">(Required)</span>
                            </label>
                            <input type="text" placeholder="Ingrese la identificacion" class="form-control" required name="identificacion_rec" id="identificacion_rec">
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="direccion_rec">DIRECCION:
                                <span class="obligatorio">(Required)</span>
                            </label>
                            <input type="text" placeholder="Ingrese la direccion" class="form-control" required name="direccion_rec" id="direccion_rec">
                        </div>
                    </div>
                </div>
            </center>
                <br>
                <center>
                    <div class="row">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="estado_rec">ESTADO:
                                    <span class="obligatorio">(Required)</span>
                                </label>
                                <input type="text" placeholder="Ingrese el estado" class="form-control" required name="estado_rec" id="estado_rec">
                            </div>
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_emision_rec">FECHA EMISION:
                                    <span class="obligatorio">(Required)</span>
                                </label>
                                <input type="text" placeholder="Ingrese la fecha de emision" class="form-control" required name="fecha_emision_rec" id="fecha_emision_rec">
                            </div>
                        </div>
                    </div>
                </center>
                    <br>
                    <center>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fecha_creacion_rec">FECHA CREACION:
                                        <span class="obligatorio">(Required)</span>
                                    </label>
                                    <input type="text" placeholder="Ingrese la fecha de creacion" class="form-control" required name="fecha_creacion_rec" id="fecha_creacion_rec">
                                </div>
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fecha_actualizacion_rec">FECHA ACTUALIZACION:
                                        <span class="obligatorio">(Required)</span>
                                    </label>
                                    <input type="text" placeholder="Ingrese la fecha de actualizacion" class="form-control" required name="fecha_actualizacion_rec" id="fecha_actualizacion_rec">
                                </div>
                            </div>
                        </div>
                    </center>
                        <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-success">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/recaudaciones/index" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban-circle"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_recaudacion").validate({
    rules:{
        numero_factura_rec:{
            required:true,
        },
        numero_autorizacion_rec:{
            required:true,
        },
        fecha_hora_autorizacion_rec:{
          required:true,
        },
        ambiente_rec:{
          required:true,
        },
        emision_rev:{
          required:true,
        },
        clave_acceso_rec:{
          required:true,
        },
        email_rec:{
          required:true,
        },
        observacion_rec:{
          required:true,
        },
        fk_id_soc:{
          required:true,
        },
        nombre_rec:{
          required:true,
        },
        identificacion_rec:{
          required:true,
        },
        direccion_rec:{
          required:true,
        },
        estado_rec:{
          required:true,
        },
        fecha_emision_rec:{
          required:true,
        },
        fecha_creacion_rec:{
          required:true,
        },
        fecha_actualizacion_rec:{
          required:true,
        }
    },
    messages:{
        numero_factura_rec:{
            required:"Porfavor, este campo solo admite numeros",
        },
        numero_autorizacion_rec:{
            required:"Porfavor, este campo solo admite numeros",
        },
        fecha_hora_autorizacion_rec:{
          required:"Porfavor, este campo solo admite numeros",
        },
        ambiente_rec:{
          required:"Porfavor, este campo solo admite letras",
        },
        emision_rev:{
          required:"Porfavor, este campo solo admite letras",
        },
        clave_acceso_rec:{
          required:"Porfavor, este campo solo admite numeros",
        },
        email_rec:{
          required:"Porfavor, este campo admite numeros, letras y caracteres especiales",
        },
        observacion_rec:{
          required:"Porfavor, este campo solo admite letras",
        },
        fk_id_soc:{
          required:"Porfavor, este campo solo admite numeros",
        },
        nombre_rec:{
          required:"Porfavor, este campo solo admite letras",
        },
        identificacion_rec:{
          required:"Porfavor, este campo solo admite numeros",
        },
        direccion_rec:{
          required:"Porfavor, este campo solo admite letras",
        },
        estado_rec:{
          required:"Porfavor, este campo solo admite numeros",
        },
        fecha_emision_rec:{
          required:"Porfavor, este campo solo admite numeros",
        },
        fecha_creacion_rec:{
          required:"Porfavor, este campo solo admite numeros",
        },
        fecha_actualizacion_rec:{
          required:"Porfavor, este campo solo admite numeros",
        }
      }
});
</script>
