<h1 class="text-center"><i class=""></i> ACTUALIZAR DATOS</h1>
<form class=""
id="frm_actualizar_registro"
action="<?php echo site_url('registros/procesarActualizacion'); ?>"
method="post"
enctype="multipart/form-data">
    <div class="row">
      <input type="hidden" name="id_reg" id="id_reg" value="<?php echo $registroEditar->id_reg; ?>">
      <div class="col-md-2"></div>
      <div class="col-md-4">
          <label for="">NOMBRE:
            <span class="obligatorio">(Required)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          required
          name="nombre_reg" value="<?php echo $registroEditar->nombre_reg; ?>"
          id="nombre_reg">
          </div>
    <div class="col-md-4">
          <label for="">APELLIDO:
            <span class="obligatorio">(Required)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el apellido"
          class="form-control"
          required
          name="apellido_reg" value="<?php echo $registroEditar->apellido_reg; ?>"
          id="apellido_reg">
          </div>

          <div class="col-md-4">
                <label for="">EDAD:
                  <span class="obligatorio">(Required)</span>
                </label>
                <br>
                <input type="text"
                placeholder="Ingrese la edad"
                class="form-control"
                required
                name="edad_reg" value="<?php echo $registroEditar->edad_reg; ?>"
                id="edad_reg">
          </div>

          <div class="col-md-4">
                <label for="">GENERO:
                  <span class="obligatorio">(Required)</span>
                </label>
                <br>
                <input type="text"
                placeholder="Ingrese el genero"
                class="form-control"
                required
                name="genero_reg" value="<?php echo $registroEditar->genero_reg; ?>"
                id="genero_reg">
          </div>

          <div class="col-md-4">
                <label for="">ESTADO CIVIL:
                  <span class="obligatorio">(Required)</span>
                </label>
                <br>
                <input type="text"
                placeholder="Ingrese el estado civil"
                class="form-control"
                required
                name="estadocivil_reg" value="<?php echo $registroEditar->estadocivil_reg; ?>"
                id="estadocivil_reg">
          </div>

          <div class="col-md-4">
                <label for="">PERSONAS HOGAR:
                  <span class="obligatorio">(Required)</span>
                </label>
                <br>
                <input type="text"
                placeholder="Ingrese el # de personas en el hogar"
                class="form-control"
                required
                name="personashogar_reg" value="<?php echo $registroEditar->personashogar_reg; ?>"
                id="personashogar_reg">
          </div>

          <div class="col-md-4">
                <label for="">DIRECCIÓN:
                  <span class="obligatorio">(Required)</span>
                </label>
                <br>
                <input type="text"
                placeholder="Ingrese la direccion"
                class="form-control"
                required
                name="dirección_reg" value="<?php echo $registroEditar->dirección_reg; ?>"
                id="dirección_reg">
          </div>

          <div class="col-md-4">
                <label for="">PARROQUIA:
                  <span class="obligatorio">(Required)</span>
                </label>
                <br>
                <input type="text"
                placeholder="Ingrese la parroquia"
                class="form-control"
                required
                name="parroquia_reg" value="<?php echo $registroEditar->parroquia_reg; ?>"
                id="parroquia_reg">
          </div>

          <div class="col-md-4">
                <label for="">BARRIO:
                  <span class="obligatorio">(Required)</span>
                </label>
                <br>
                <input type="text"
                placeholder="Ingrese el barrio"
                class="form-control"
                required
                name="barrio_reg" value="<?php echo $registroEditar->barrio_reg; ?>"
                id="barrio_reg">
          </div>

          <div class="col-md-4">
                <label for="">VIVIENDA:
                  <span class="obligatorio">(Required)</span>
                </label>
                <br>
                <input type="text"
                placeholder="Ingrese la vivienda"
                class="form-control"
                required
                name="vivienda_reg" value="<?php echo $registroEditar->vivienda_reg; ?>"
                id="vivienda_reg">
          </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br><br>
            <button type="submit" name="button"
            class="btn btn-success">
            <i class="glyphicon glyphicon-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/registros/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-ban-circle"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_actualizar_registros").validate({
   rules:{
     nombre_reg:{
       required:true
     },
     apellido_reg:{
       required:true
     },
     edad_reg:{
       required:true
     },
     genero_reg:{
       required:true
     },
     estadocivil_reg:{
       required:true
     },
     personashogar_reg:{
       required:true
     },
     dirección_reg:{
       required:true
     },
     parroquia_reg:{
       required:true
     },
     barrio_reg:{
       required:true
     },
     vivienda_reg:{
       required:true
     }
   },
   messages:{
     nombre_reg:{
         required:"Porfavor, este campo solo admite letras",
     },
     apellido_reg:{
         required:"Porfavor, este campo solo admite letras",
     },
     edad_reg:{
       required:"Porfavor, este campo solo admite numeros",
     },
     genero_reg:{
       required:"Porfavor, este campo solo admite letras",
     },
     estadocivil_reg:{
       required:"Porfavor, este campo solo admite letras",
     },
     personashogar_reg:{
       required:"Porfavor, este campo solo admite numeros",
     },
     dirección_reg:{
       required:"Porfavor, este campo solo admite letras",
     },
     parroquia_reg:{
       required:"Porfavor, este campo solo admite letras",
     },
     barrio_reg:{
       required:"Porfavor, este campo solo admite letras",
     },
     vivienda_reg:{
       required:"Porfavor, este campo solo admite letras",
     }
   }
 });

 </script>
