<style>
  .card {
    border: 1px solid white;
  }
</style>

<?php
$totalRegistros = 0;
$mostSeatsStadium = null;

if ($listadoRegistros) {
    $totalRegistros = sizeof($listadoRegistros);
    $maxSeats = PHP_INT_MIN;

    foreach ($listadoRegistros as $registroTemporal) {
    }
}
?>
<div class="row">
  <div class="col-md-2">
    <center>
    <img src="<?php echo base_url(); ?>/assets/image/logoicon.png" alt="" width="80px" height="auto">
  </center>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE PERSONAS</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('registros/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i> Agregar Personal</a>
</center>
<br>
</div>
<br>
<?php if ($listadoRegistros): ?>

  <div class="table-responsive" style="margin: 2 120px">
    <table class="table table-striped table-bordered table-hover" id="tbl_registros">
      <thead>
        <tr>
          <th>ID</th>
          <th>NOMBRE</th>
          <th>APELLIDO</th>
          <th>EDAD</th>
          <th>GENERO</th>
          <th>ESTADOCIVIL</th>
          <th># PERSONAS</th>
          <th>DIRECCION</th>
          <th>PARROQUIA</th>
          <th>BARRIO</th>
          <th>VIVIENDA</th>
          <th>BOTT</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoRegistros as $registroTemporal): ?>
          <tr>
            <td><?php echo $registroTemporal->id_reg ?></td>
            <td><?php echo $registroTemporal->nombre_reg ?></td>
            <td><?php echo $registroTemporal->apellido_reg ?></td>
            <td><?php echo $registroTemporal->edad_reg ?></td>
            <td><?php echo $registroTemporal->genero_reg ?></td>
            <td><?php echo $registroTemporal->estadocivil_reg ?></td>
            <td><?php echo $registroTemporal->personashogar_reg ?></td>
            <td><?php echo $registroTemporal->dirección_reg ?></td>
            <td><?php echo $registroTemporal->parroquia_reg ?></td>
            <td><?php echo $registroTemporal->barrio_reg ?></td>
            <td><?php echo $registroTemporal->vivienda_reg ?></td>

            <td class="text-center">
              <a href="<?php echo site_url(); ?>/registros/actualizar/<?php echo $registroTemporal->id_reg; ?>" title="Editar Registro">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  EDIT.
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/registros/borrar/<?php echo $registroTemporal->id_reg; ?>" title="Eliminar registro" onclick="return confirm('Are you sure to delete permanently?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  DELET
                </button>
              </a>
            </td>



          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>There are no places</h1>
<?php endif; ?>

<br>
<div class="row" style="margin: 0 120px;">


  <div class="col-md-4">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          <img src="<?php echo base_url(); ?>/assets/image/line.png" alt="" width="250" height="200">
          <?php echo $totalRegistros; ?>
        </h5>
        <p class="card-text">REGISTERED PLACES</p>
      </div>
    </div>
  </div>
</div>
<br>

<script type="text/javascript">
  $("#tbl_registros").DataTable();
</script>
