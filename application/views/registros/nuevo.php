<div class="row">
  <div class="col-md-2">
    <center>
    <img src="<?php echo base_url(); ?>/assets/image/logoicon.png" alt="" width="80px" height="auto">
  </center>
  </div>
</div>
<br>
<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVO REGISTRO</h1>
<form class="" id="frm_nuevo_registro" action="<?php echo site_url('registros/guardarRegistro'); ?>" method="post" enctype="multipart/form-data">
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="nombre_reg">NOMBRES:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese nombres completos" class="form-control" required name="nombre_reg" id="nombre_reg">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="apellido_reg">APELLIDOS:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese apellidos completos" class="form-control" required name="apellido_reg" id="apellido_reg">
            </div>
        </div>
    </div>
</center>
    <br>
    <center>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label for="edad_reg">EDAD:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la edad" class="form-control" required name="edad_reg" id="edad_reg">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="genero_reg">INGRESE EL GÉNERO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el género" class="form-control" required name="genero_reg" id="genero_reg">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="estadocivil_reg">INGRESE EL ESTADO CIVIL:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el estado civil" class="form-control" required name="estadocivil_reg" id="estadocivil_reg">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="personashogar_reg"># PERSONAS:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el # de personas en el hogar" class="form-control" required name="personashogar_reg" id="personashogar_reg">
            </div>
        </div>
    </div>
</center>
    <br>

    <center>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label for="dirección_reg">DIRECCIÓN:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese su dirección" class="form-control" required name="dirección_reg" id="dirección_reg">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="parroquia_reg">PARROQUIA:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la parroquia" class="form-control" required name="parroquia_reg" id="parroquia_reg">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="barrio_reg">BARRIO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el barrio" class="form-control" required name="barrio_reg" id="barrio_reg">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="vivienda_reg">VIVIENDA:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la vivienda" class="form-control" required name="vivienda_reg" id="vivienda_reg">
            </div>
        </div>
    </div>
</center>
    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-success">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/registros/index" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban-circle"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_registro").validate({
    rules:{
        nombre_reg:{
            required:true,
        },
        apellido_reg:{
            required:true,
        },
        edad_reg:{
          required:true,
        },
        genero_reg:{
          required:true,
        },
        estadocivil_reg:{
          required:true,
        },
        personashogar_reg:{
          required:true,
        },
        dirección_reg:{
          required:true,
        },
        parroquia_reg:{
          required:true,
        },
        barrio_reg:{
          required:true,
        },
        vivienda_reg:{
          required:true,
        }
    },
    messages:{
        nombre_reg:{
            required:"Porfavor, este campo solo admite letras",
        },
        apellido_reg:{
            required:"Porfavor, este campo solo admite letras",
        },
        edad_reg:{
          required:"Porfavor, este campo solo admite numeros",
        },
        genero_reg:{
          required:"Porfavor, este campo solo admite letras",
        },
        estadocivil_reg:{
          required:"Porfavor, este campo solo admite letras",
        },
        personashogar_reg:{
          required:"Porfavor, este campo solo admite numeros",
        },
        dirección_reg:{
          required:"Porfavor, este campo solo admite letras",
        },
        parroquia_reg:{
          required:"Porfavor, este campo solo admite letras",
        },
        barrio_reg:{
          required:"Porfavor, este campo solo admite letras",
        },
        vivienda_reg:{
          required:"Porfavor, este campo solo admite letras",
        }

      }
});
</script>
