<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVA ASISTENCIAS</h1>
<form class="" id="frm_nuevo_asistencia" action="<?php echo site_url('asistencias/guardarAsistencia'); ?>" method="post" enctype="multipart/form-data">

<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="fk_id_eve">FK_ID_EVE:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el id de evento" class="form-control" required name="fk_id_eve" id="fk_id_eve">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="fk_id_soc">FK_ID_SOC:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el id de socio" class="form-control" required name="fk_id_soc" id="fk_id_soc">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="tipo_asi">TIPO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el tipo " class="form-control" required name="tipo_asi" id="tipo_asi">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="valor_asi">VALOR:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el valor" class="form-control" required name="valor_asi" id="valor_asi">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="atraso_asi">ATRASO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el atraso" class="form-control" required name="atraso_asi" id="atraso_asi">
            </div>
        </div>
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="valor_atraso_asi">VALOR ATRASO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el valor del atraso" class="form-control" required name="valor_atraso_asi" id="valor_atraso_asi">
            </div>
        </div>
    </div>
</center>
    <br>
    <center>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="creacion_asi">CREACION:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese la fecha de creacion" class="form-control" required name="creacion_asi" id="creacion_asi">
                </div>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="actualizacion_asi">ACTUALIZACION:
                        <span class="obligatorio">(Required)</span>
                    </label>
                    <input type="text" placeholder="Ingrese la fecha de actualizacion" class="form-control" required name="actualizacion_asi" id="actualizacion_asi">
                </div>
            </div>
        </div>
    </center>
    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-success">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/registros/index" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban-circle"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_asistencia").validate({
    rules:{
        fk_id_eve:{
            required:true,
        },
        fk_id_soc:{
            required:true,
        },
        tipo_asi:{
          required:true,
        },
        valor_asi:{
          required:true,
        },
        atraso_asi:{
          required:true,
        },
        valor_atraso_asi:{
          required:true,
        },
        creacion_asi:{
          required:true,
        },
        actualizacion_asi:{
          required:true,
        }
    },
    messages:{
        fk_id_eve:{
            required:"Porfavor, este campo solo acepta numero",
        },
        fk_id_soc:{
            required:"Porfavor, este campo solo acepta numero",
        },
        tipo_asi:{
          required:"Porfavor, este campo solo acepta letras",
        },
        valor_asi:{
          required:"Porfavor, este campo solo acepta numero",
        },
        atraso_asi:{
          required:"Porfavor, este campo solo acepta letras",
        },
        valor_atraso_asi:{
          required:"Porfavor, este campo solo acepta numero",
        },
        creacion_asi:{
          required:"Porfavor, este campo solo acepta numero",
        },
        actualizacion_asi:{
          required:"Porfavor, este campo solo acepta numero",
        }
      }
});
</script>
