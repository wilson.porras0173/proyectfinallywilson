<h1 class="text-center"><i class=""></i> ACTUALIZAR DATOS</h1>
<form class=""
id="frm_actualizar_historialpropietario"
action="<?php echo site_url('historialpropietarios/procesarActualizacion'); ?>"
method="post"
enctype="multipart/form-data">
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
          <div class="col-md-4">
            <input type="hidden" name="id_his" id="id_his" value="<?php echo $historialpropietarioEditar->id_his; ?>">
              <label for="">FK_ID_MED:
                <span class="obligatorio">(Required)</span>
              </label>
              <br>
              <input type="text"
              placeholder="Enter the name of the place"
              class="form-control"
              required
              name="fk_id_med" value="<?php echo $historialpropietarioEditar->fk_id_med; ?>"
              id="fk_id_med">
              </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="fk_id_soc">FK_ID_SOC:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese la descripcion" class="form-control" required name="fk_id_soc"  value="<?php echo $historialpropietarioEditar->fk_id_soc; ?>" id="fk_id_soc">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="actualizacion_his">ACTUALIZACION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el estado" class="form-control" required name="actualizacion_his" value="<?php echo $historialpropietarioEditar->actualizacion_his; ?>" id="actualizacion_his">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="estado_his">ESTADO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese apellidos completos" class="form-control" required name="estado_his" value="<?php echo $historialpropietarioEditar->estado_his; ?>" id="estado_his">
            </div>
        </div>
    </div>
</center>
<br>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="observacion_his">OBSERVACION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el estado" class="form-control" required name="observacion_his" value="<?php echo $historialpropietarioEditar->observacion_his; ?>" id="observacion_his">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha_cambio_his">FECHA CAMBIO:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese apellidos completos" class="form-control" required name="fecha_cambio_his" value="<?php echo $historialpropietarioEditar->fecha_cambio_his; ?>" id="fecha_cambio_his">
            </div>
        </div>
    </div>
</center>
<br>
<br>
<center>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="creacion_his">CREACION:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese el estado" class="form-control" required name="creacion_his" value="<?php echo $historialpropietarioEditar->creacion_his; ?>" id="creacion_his">
            </div>
        </div>
        <div class="col-md-1">
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="propietario_actual_his">PROPIETARIO ACTUAL:
                    <span class="obligatorio">(Required)</span>
                </label>
                <input type="text" placeholder="Ingrese apellidos completos" class="form-control" required name="propietario_actual_his" value="<?php echo $historialpropietarioEditar->propietario_actual_his; ?>" id="propietario_actual_his">
            </div>
        </div>
    </div>
</center>
<br>
<center>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br><br>
            <button type="submit" name="button"
            class="btn btn-success">
            <i class="glyphicon glyphicon-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/historialpropietarios/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-ban-circle"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_actualizar_historialpropietario").validate({
   rules:{
     fk_id_med:{
         required:true,
     },
     fk_id_soc:{
       required:true,
     },
     actualizacion_his:{
       required:true,
     },
     estado_his:{
       required:true,
     },
     observacion_his:{
       required:true,
     },
     fecha_cambio_his:{
       required:true,
     },
     creacion_his:{
       required:true,
     },
     propietario_actual_his:{
       required:true,
     }
   },
   messages:{
     fk_id_med:{
         required:"Porfavor, este campo solo admite numeros",
     },
     fk_id_soc:{
         required:"Porfavor, este campo solo admite numeros",
     },
     actualizacion_his:{
       required:"Porfavor, este campo solo admite numeros",
     },
     estado_his:{
       required:"Porfavor, este campo solo admite letras",
     },
     observacion_his:{
       required:"Porfavor, este campo solo admite letras",
     },
     fecha_cambio_his:{
       required:"Porfavor, este campo solo admite numeros",
     },
     creacion_his:{
       required:"Porfavor, este campo solo admite numeros",
     },
     propietario_actual_his:{
       required:"Porfavor, este campo solo admite letras",
     }
   }
 });

 </script>
