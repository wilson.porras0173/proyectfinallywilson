<?php
   class Historialpropietario extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("historial_propietario",$datos);
     }
     //Funcion que consulta todos los historial_propietario de la bdd
     public function obtenerTodos(){
        $this->db->order_by("id_his","asc");
        $result=$this->db->get("historial_propietario");
        if ($result->num_rows()>0) {
          return $result->result();
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un historial_propietario se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_his",$id);
        return $this->db->delete("historial_propietario");
     }
     //Consultando el historial_propietario por su id
     public function obtenerPorId($id){
        $this->db->where("id_his",$id);
        $historialpropietario=$this->db->get("historial_propietario");
        if($historialpropietario->num_rows()>0){
          return $historialpropietario->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de historial_propietario
     public function actualizar($id,$datos){
       $this->db->where("id_his",$id);
       return $this->db->update("historial_propietario",$datos);
     }

   }//Cierre de la clase (No borrar)














//
