<?php
   class Detalle extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("detalle",$datos);
     }
     //Funcion que consulta todos los registros de la bdd
     public function obtenerTodos(){
        $this->db->order_by("id_det","asc");
        $result=$this->db->get("detalle");
        if ($result->num_rows()>0) {
          return $result->result();
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un registro se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_det",$id);
        return $this->db->delete("detalle");
     }
     //Consultando el registro por su id
     public function obtenerPorId($id){
        $this->db->where("id_det",$id);
        $detalle=$this->db->get("detalle");
        if($detalle->num_rows()>0){
          return $detalle->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de registro
     public function actualizar($id,$datos){
       $this->db->where("id_det",$id);
       return $this->db->update("detalle",$datos);
     }

   }//Cierre de la clase (No borrar)














//
