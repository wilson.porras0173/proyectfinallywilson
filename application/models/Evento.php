<?php
   class Evento extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("evento",$datos);
     }
     //Funcion que consulta todos los registros de la bdd
     public function obtenerTodos(){
        $this->db->order_by("id_eve","asc");
        $result=$this->db->get("evento");
        if ($result->num_rows()>0) {
          return $result->result();
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un registro se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_eve",$id);
        return $this->db->delete("evento");
     }
     //Consultando el registro por su id
     public function obtenerPorId($id){
        $this->db->where("id_eve",$id);
        $evento=$this->db->get("evento");
        if($evento->num_rows()>0){
          return $evento->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de registro
     public function actualizar($id,$datos){
       $this->db->where("id_eve",$id);
       return $this->db->update("evento",$datos);
     }

   }//Cierre de la clase (No borrar)














//
