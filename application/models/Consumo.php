<?php
   class Consumo extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("consumo",$datos);
     }
     //Funcion que consulta todos los consumo de la bdd
     public function obtenerTodos(){
        $this->db->order_by("id_consumo","asc");
        $result=$this->db->get("consumo");
        if ($result->num_rows()>0) {
          return $result->result();
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un consumo se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_consumo",$id);
        return $this->db->delete("consumo");
     }
     //Consultando el consumo por su id
     public function obtenerPorId($id){
        $this->db->where("id_consumo",$id);
        $consumo=$this->db->get("consumo");
        if($consumo->num_rows()>0){
          return $consumo->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de consumo
     public function actualizar($id,$datos){
       $this->db->where("id_consumo",$id);
       return $this->db->update("consumo",$datos);
     }

   }//Cierre de la clase (No borrar)














//
