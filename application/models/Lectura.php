<?php
   class Lectura extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("lectura",$datos);
     }
     //Funcion que consulta todos los registros de la bdd
     public function obtenerTodos(){
        $this->db->order_by("id_lec","asc");
        $result=$this->db->get("lectura");
        if ($result->num_rows()>0) {
          return $result->result();
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un registro se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_lec",$id);
        return $this->db->delete("lectura");
     }
     //Consultando el registro por su id
     public function obtenerPorId($id){
        $this->db->where("id_lec",$id);
        $lectura=$this->db->get("lectura");
        if($lectura->num_rows()>0){
          return $lectura->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de registro
     public function actualizar($id,$datos){
       $this->db->where("id_lec",$id);
       return $this->db->update("lectura",$datos);
     }

   }//Cierre de la clase (No borrar)














//
