<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lecturas extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("lectura");
	}
	//renderiza la vista index de registros
	public function index()
	{
		$data["listadoLecturas"]=
		$this->lectura->obtenerTodos();
		$this->load->view('header');
		$this->load->view('lecturas/index',$data);
		$this->load->view('footer');
	}
 //renderiza la vista nuevo de registros
	public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('lecturas/nuevo');
		$this->load->view('footer');
	}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardarLectura(){
		$datosNuevoLectura=array(
      "anio_lec"=>$this->input->post('anio_lec'),
			"mes_lec"=>$this->input->post('mes_lec'),
			"estado_lec"=>$this->input->post('estado_lec'),
      "lectura_anterior_lec"=>$this->input->post('lectura_anterior_lec'),
      "lectura_actual_lec"=>$this->input->post('lectura_actual_lec'),
      "fecha_creacion_lec"=>$this->input->post('fecha_creacion_lec'),
      "fecha_actualizacion_lec"=>$this->input->post('fecha_actualizacion_lec'),
      "fk_id_his"=>$this->input->post('fk_id_his'),
      "fk_id_consumo"=>$this->input->post('fk_id_consumo')
		);
    if($this->lectura->insertar($datosNuevoLectura)){
				$this->session
				->set_flashdata('confirmacion',
			 'Lectura insertado exitosamente');
		}else{
			$this->session
			->set_flashdata('error',
		 'Error al insertar, verifique e intente de nuevo');
		}
		redirect('lecturas/index');
	}
	//funcion para eliminar registros
	public function borrar($id_lec){
		if ($this->lectura->eliminarPorId($id_lec)){
			$this->session
			->set_flashdata('confirmacion',
		 'Lectura ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('lecturas/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["lecturaEditar"]=
			$this->lectura->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("lecturas/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosLecturaEditado=array(
      "anio_lec"=>$this->input->post('anio_lec'),
			"mes_lec"=>$this->input->post('mes_lec'),
			"estado_lec"=>$this->input->post('estado_lec'),
      "lectura_anterior_lec"=>$this->input->post('lectura_anterior_lec'),
      "lectura_actual_lec"=>$this->input->post('lectura_actual_lec'),
      "fecha_creacion_lec"=>$this->input->post('fecha_creacion_lec'),
      "fecha_actualizacion_lec"=>$this->input->post('fecha_actualizacion_lec'),
      "fk_id_his"=>$this->input->post('fk_id_his'),
      "fk_id_consumo"=>$this->input->post('fk_id_consumo')
		);
	 $id=$this->input->post("id_lec");
		if($this->lectura->actualizar($id,$datosLecturaEditado)){
			redirect('lecturas/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}// cierre de la clase (No borrar)




//
