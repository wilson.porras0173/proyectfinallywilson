<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historialpropietarios extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("historialpropietario");
	}
	//renderiza la vista index de historialpropietario
	public function index()
	{
		$data["listadoHistorialpropietarios"]=
		$this->historialpropietario->obtenerTodos();
		$this->load->view('header');
		$this->load->view('historialpropietarios/index',$data);
		$this->load->view('footer');
	}
 //renderiza la vista nuevo de historialpropietario
	public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('historialpropietarios/nuevo');
		$this->load->view('footer');
	}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardarHistorialpropietario(){
		$datosNuevoHistorialpropietario=array(
			"fk_id_med"=>$this->input->post('fk_id_med'),
      "fk_id_soc"=>$this->input->post('fk_id_soc'),
			"actualizacion_his"=>$this->input->post('actualizacion_his'),
      "estado_his"=>$this->input->post('estado_his'),
			"observacion_his"=>$this->input->post('observacion_his'),
			"fecha_cambio_his"=>$this->input->post('fecha_cambio_his'),
			"creacion_his"=>$this->input->post('creacion_his'),
			"propietario_actual_his"=>$this->input->post('propietario_actual_his')
		);
    if($this->historialpropietario->insertar($datosNuevoHistorialpropietario)){
				$this->session
				->set_flashdata('confirmacion',
			 'Evento insertado exitosamente');
		}else{
			$this->session
			->set_flashdata('error',
		 'Error al insertar, verifique e intente de nuevo');
		}
		redirect('historialpropietarios/index');
	}
	//funcion para eliminar historialpropietario
	public function borrar($id_his){
		if ($this->historialpropietario->eliminarPorId($id_his)){
			$this->session
			->set_flashdata('confirmacion',
		 'Evento ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('historialpropietarios/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["historialpropietarioEditar"]=
			$this->historialpropietario->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("historialpropietarios/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosHistorialpropietarioEditado=array(
			"fk_id_med"=>$this->input->post('fk_id_med'),
      "fk_id_soc"=>$this->input->post('fk_id_soc'),
			"actualizacion_his"=>$this->input->post('actualizacion_his'),
      "estado_his"=>$this->input->post('estado_his'),
			"observacion_his"=>$this->input->post('observacion_his'),
			"fecha_cambio_his"=>$this->input->post('fecha_cambio_his'),
			"creacion_his"=>$this->input->post('creacion_his'),
			"propietario_actual_his"=>$this->input->post('propietario_actual_his')
		);
	 $id=$this->input->post("id_his");
		if($this->historialpropietario->actualizar($id,$datosHistorialpropietarioEditado)){
			redirect('historialpropietarios/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}// cierre de la clase (No borrar)




//
