<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Socios extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("socio");
	}
	//renderiza la vista index de registros
	public function index()
	{
		$data["listadoSocios"]=
		$this->socio->obtenerTodos();
		$this->load->view('header');
		$this->load->view('socios/index',$data);
		$this->load->view('footer');
	}
 //renderiza la vista nuevo de registros
	public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('socios/nuevo');
		$this->load->view('footer');
	}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardarSocio(){
		$datosNuevoSocio=array(
      "tipo_soc"=>$this->input->post('tipo_soc'),
			"identificacion_soc"=>$this->input->post('identificacion_soc'),
			"primer_apellido_soc"=>$this->input->post('primer_apellido_soc'),
      "segundo_apellido_soc"=>$this->input->post('segundo_apellido_soc'),
      "nombres_soc"=>$this->input->post('nombres_soc'),
      "email_soc"=>$this->input->post('email_soc'),
      "foto_soc"=>$this->input->post('foto_soc'),
      "telefono_soc"=>$this->input->post('telefono_soc'),
      "direccion_soc"=>$this->input->post('direccion_soc'),
      "fecha_nacimiento_soc"=>$this->input->post('fecha_nacimiento_soc'),
      "discapacidad_soc"=>$this->input->post('discapacidad_soc'),
      "fk_id_usu"=>$this->input->post('fk_id_usu'),
      "estado_soc"=>$this->input->post('estado_soc')
		);
    if($this->socio->insertar($datosNuevoSocio)){
				$this->session
				->set_flashdata('confirmacion',
			 'Socio insertado exitosamente');
		}else{
			$this->session
			->set_flashdata('error',
		 'Error al insertar, verifique e intente de nuevo');
		}
		redirect('socios/index');
	}
	//funcion para eliminar registros
	public function borrar($id_soc){
		if ($this->socio->eliminarPorId($id_soc)){
			$this->session
			->set_flashdata('confirmacion',
		 'Socio ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('socios/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["socioEditar"]=
			$this->socio->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("socios/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosSocioEditado=array(
      "tipo_soc"=>$this->input->post('tipo_soc'),
			"identificacion_soc"=>$this->input->post('identificacion_soc'),
			"primer_apellido_soc"=>$this->input->post('primer_apellido_soc'),
      "segundo_apellido_soc"=>$this->input->post('segundo_apellido_soc'),
      "nombres_soc"=>$this->input->post('nombres_soc'),
      "email_soc"=>$this->input->post('email_soc'),
      "foto_soc"=>$this->input->post('foto_soc'),
      "telefono_soc"=>$this->input->post('telefono_soc'),
      "direccion_soc"=>$this->input->post('direccion_soc'),
      "fecha_nacimiento_soc"=>$this->input->post('fecha_nacimiento_soc'),
      "discapacidad_soc"=>$this->input->post('discapacidad_soc'),
      "fk_id_usu"=>$this->input->post('fk_id_usu'),
      "estado_soc"=>$this->input->post('estado_soc')
		);
	 $id=$this->input->post("id_soc");
		if($this->socio->actualizar($id,$datosSocioEditado)){
			redirect('socios/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}// cierre de la clase (No borrar)




//
